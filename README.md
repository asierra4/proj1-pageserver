# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

### Assignment ###
* Add the following functionality in pageserver.py. (a) If URL ends with `name.html` or `name.css` (i.e., if `path/to/name.html` is in document path (from DOCROOT)), send content of `name.html` or `name.css` with proper http response. (b) If `name.html` is not in current directory Respond with 404 (not found). (c) If a page starts with one of the symbols(~ // ..), respond with 403 forbidden error. For example, `url=localhost:5000/..name.html` or `/~name.html` would give 403 forbidden error.
  
  ```
  ## Author: Adrian Sierra, asierra4@uoregon.edu ##
  ```
